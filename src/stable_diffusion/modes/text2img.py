import gradio as gr
import torch
from diffusers import EulerDiscreteScheduler, StableDiffusionPipeline
from PIL import Image
from torch import autocast

from stable_diffusion.settings import SERVER_NAME

MODEL_NAME = "stabilityai/stable-diffusion-2"

DEVICE = "cuda"
IMAGE_COUNT = 4


def run_text2img():
    generator = ImageGenerator()

    positive_prompt = gr.Textbox(label="prompt")
    negative_prompt = gr.Textbox(label="negative prompt")
    image_count = gr.Slider(minimum=1, maximum=4, label="images")
    gallery = gr.Gallery(
        label="Generated images", show_label=False, elem_id="gallery"
    ).style(grid=[2], height="auto")

    title = "Text2Img Stable Diffusion CPU"
    description = "Stable Diffusion text 2 image"

    gr.Interface(
        fn=generator.infer,
        inputs=[positive_prompt, negative_prompt, image_count],
        outputs=gallery,
        title=title,
        description=description,
    ).launch(share=False, server_name=SERVER_NAME)


class ImageGenerator:
    def __init__(self) -> None:
        scheduler = EulerDiscreteScheduler.from_pretrained(
            MODEL_NAME, subfolder="scheduler"
        )
        pipe = StableDiffusionPipeline.from_pretrained(
            MODEL_NAME, scheduler=scheduler, revision="fp16", torch_dtype=torch.float16
        )
        pipe = pipe.to(DEVICE)
        pipe.safety_checker = None
        self.pipe = pipe

    @autocast(DEVICE)
    def infer(
        self, prompt: str, negative_prompt: str, image_count: int
    ) -> list[Image.Image]:
        return [
            image
            for _ in range(image_count)
            for image in self.pipe(
                prompt,
                negative_prompt=negative_prompt,
                num_images_per_prompt=1,
                height=768,
                width=768,
            ).images
        ]
