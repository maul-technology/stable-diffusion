import gradio as gr
import torch
from diffusers import DiffusionPipeline
from PIL import Image
from torch import autocast

from stable_diffusion.image import ImageWrapper
from stable_diffusion.settings import DEVICE, SERVER_NAME

MODEL_NAME = "runwayml/stable-diffusion-inpainting"
IMAGE_COUNT = 4


def run_inpainting():
    generator = ImageGenerator()

    source_image = gr.Image(
        source="upload",
        tool="sketch",
        image_mode="RGB",
        type="pil",
        label="Upload",
        elem_id="image_upload",
    )
    positive_prompt = gr.Textbox(label="prompt")
    negative_prompt = gr.Textbox(label="negative prompt")
    gallery = gr.Gallery(
        label="Generated images",
        show_label=False,
    ).style(grid=[2], height="auto")

    title = "Inpainting Stable Diffusion"
    description = "Stable Diffusion Image Inpainting"

    gr.Interface(
        fn=generator.infer,
        inputs=[
            source_image,
            positive_prompt,
            negative_prompt,
        ],
        outputs=gallery,
        title=title,
        description=description,
    ).launch(share=False, server_name=SERVER_NAME)


class ImageGenerator:
    def __init__(self) -> None:
        pipe = DiffusionPipeline.from_pretrained(
            MODEL_NAME,
            torch_dtype=torch.float16,
        )
        pipe.to(DEVICE)
        pipe.safety_checker = None
        self.pipe = pipe

    @autocast("cuda")
    def infer(
        self, image: dict[str, Image.Image], prompt: str, negative_prompt: str
    ) -> list[Image.Image]:
        source_image = ImageWrapper.make(image["image"]).resize_fill()
        mask = ImageWrapper.make(image["mask"]).resize_fill().image
        mask = mask.convert("L")

        return [
            source_image.merge(result, mask)
            for result in self.pipe(
                prompt=prompt,
                negative_prompt=negative_prompt,
                num_images_per_prompt=IMAGE_COUNT,
                image=source_image.image,
                mask_image=mask,
                guidance_scale=7.5,
            ).images
        ]
