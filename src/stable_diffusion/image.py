from __future__ import annotations

from dataclasses import dataclass

from PIL import Image, ImageOps


@dataclass
class ImageWrapper:
    image: Image.Image
    size: (int, int)

    @staticmethod
    def make(image: Image.Image) -> ImageWrapper:
        image = image.convert("RGB")
        return ImageWrapper(image=image, size=image.size)

    def resize_fill(self, size: int = 512) -> ImageWrapper:
        """
        Scale the image, maintaining aspect ratio, and then fill the extra
        space with black to make square
        """
        assert self.size == self.image.size

        width, height = self.image.size
        ratio = min(size / width, size / height)
        width = int(width * ratio)
        height = int(height * ratio)

        image = self.image.resize((width, height), Image.Resampling.LANCZOS)
        new_image = Image.new("RGB", (size, size))
        box = tuple(
            (new - original) // 2 for new, original in zip(new_image.size, image.size)
        )
        new_image.paste(image, box)

        return ImageWrapper(image=new_image, size=image.size)

    def crop(self, image: Image.Image) -> Image.Image:
        """Restore the image to the original size"""
        assert image.size[0] == image.size[1]
        size, _ = image.size
        width, height = self.size
        x_offset = (size - width) // 2
        y_offset = (size - height) // 2

        box = (
            x_offset,
            y_offset,
            size - x_offset,
            size - y_offset,
        )
        return image.crop(box)

    def merge(self, image: Image.Image, mask: Image.Image) -> Image.Image:
        composite = Image.composite(self.image, image, ImageOps.invert(mask))
        return self.crop(composite)
