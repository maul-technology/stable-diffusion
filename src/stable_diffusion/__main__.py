import typer

from stable_diffusion.modes.depth2img import run_depth2img
from stable_diffusion.modes.img2img import run_img2img
from stable_diffusion.modes.inpainting import run_inpainting
from stable_diffusion.modes.text2img import run_text2img

app = typer.Typer()


@app.command()
def depth2img():
    run_depth2img()


@app.command()
def img2img():
    run_img2img()


@app.command()
def inpainting():
    run_inpainting()


@app.command()
def text2img():
    run_text2img()


if __name__ == "__main__":
    app()
