import json
from pathlib import Path

AUTH_FILE = Path.home() / ".config" / "huggingface" / "auth.json"
ACCESS_TOKEN = json.loads(AUTH_FILE.read_text())["access_token"]
