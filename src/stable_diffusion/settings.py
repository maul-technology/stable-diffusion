import os

import torch

DEVICE = os.environ.get("DEVICE", "cuda" if torch.cuda.is_available() else "cpu")
SERVER_NAME = os.environ.get("SERVER_NAME", "dl.matthew.lan")
MODEL_NAME = os.environ.get("MODEL_NAME")
