import gradio as gr
import torch
from diffusers import StableDiffusionDepth2ImgPipeline
from PIL import Image
from torch import autocast

from stable_diffusion.image import ImageWrapper
from stable_diffusion.settings import DEVICE, SERVER_NAME

MODEL_NAME = "runwayml/stable-diffusion-v1-5"
IMAGE_COUNT = 4


def run_depth2img():
    generator = ImageGenerator()

    source_image = gr.Image(source="upload", type="pil", label="init_img | 512*512 px")
    positive_prompt = gr.Textbox(label="prompt")
    negative_prompt = gr.Textbox(label="negative prompt")
    gallery = gr.Gallery(
        label="Generated images", show_label=False, elem_id="gallery"
    ).style(grid=[2], height="auto")

    title = "Depth2Img Stable Diffusion CPU"
    description = "Stable Diffusion depth 2 image"

    gr.Interface(
        fn=generator.infer,
        inputs=[
            source_image,
            positive_prompt,
            negative_prompt,
            gr.Slider(2, 15, value=7, label="Guidence Scale"),
            gr.Slider(label="Strength", minimum=0, maximum=1, step=0.05, value=0.75),
        ],
        outputs=gallery,
        title=title,
        description=description,
    ).launch(share=False, server_name=SERVER_NAME)


class ImageGenerator:
    def __init__(self) -> None:
        pipe = StableDiffusionDepth2ImgPipeline.from_pretrained(
            MODEL_NAME, torch_dtype=torch.float16
        )
        pipe.to(DEVICE)
        pipe.safety_checker = None
        self.pipe = pipe

    @autocast(DEVICE)
    def infer(
        self,
        source_image: Image.Image,
        prompt: str,
        negative_prompt: str,
        guidance_scale: float,
        strength: float,
    ) -> list[Image.Image]:
        source_image = ImageWrapper.make(source_image).resize_fill()
        return [
            source_image.crop(result)
            for result in self.pipe(
                prompt,
                negative_prompt=negative_prompt,
                num_images_per_prompt=IMAGE_COUNT,
                init_image=source_image.image,
                strength=strength,
                guidance_scale=guidance_scale,
                num_inference_steps=50,
            ).images
        ]
